<?php

namespace Vera\Wilson;

/**
 * Calculates the lower bound of Wilson
 * @see http://www.evanmiller.org/how-not-to-sort-by-average-rating.html
 */
class LowerBound {

    /**
     * Confidence interval for the lower bound \
     * 95 by default
     */
    public $confidence;

    /**
     * Quantile of the standard normal distribution
     */
    public $z;

    /**
     * Total number of ratings \
     */
    public $n;

    /**
     * Total number of positive ratings \
     */
    public $positive;

    /**
     * Positive results out of total results
     * `^p`
     */
    public $p;

    /**
     * Total number of negative ratings \
     * `neg`
     */
    public $negative;

    public function __construct() {
        $this->setConfidence();
    }

    /**
     * Set the confidence percentage to calculate
     * @param float $confidence Percentage as float, 0 to 1
     */
    public function setConfidence(float $confidence = 0.95) {
        if ($confidence > 1) throw new \Exception("Confidence must be defined as a float between 0 to 1.");

        $this->confidence = $confidence * 100;

        $this->z = $this->pnorm(1 - (1-$confidence) / 2);
    }

    /**
     * Pass ratings
     * @param int $positive Total number of positive ratings
     * @param int $negative Total number of negative ratings
     */
    public function addRatings(int $positive, int $negative) {
        $this->positive = $positive;
        $this->negative = $negative;
        $this->n = $positive + $negative;
        if ($this->n > 0) $this->p = $positive / $this->n;
    }
    
    /**
     * Get lower bound score
     * @return float
     */
    public function getLowerBound() {
        if ($this->n < 1) return 0;
        return $this->multiply() * ($this->innerLeft() - $this->innerRight());
    }

    /**
     * Get upper bound score
     * @return float
     */
    public function getUpperBound() {
        if ($this->n < 1) return 0;
        return $this->multiply() * ($this->innerLeft() + $this->innerRight()); 
    }

    /**
     * Get lower bound score
     * @return array Interval from lower to upper bond
     */
    public function getScore() {
        return [
            'lowerBound' => $this->getLowerBound(),
            'upperBound' => $this->getUpperBound(),
            'boundInterval' => $this->getUpperBound() - $this->getLowerBound()
        ];
    }

    /**
     * Translated from ruby Statistics2.pnorm
     * @param $quantile
     * @return float
     * @see https://github.com/abscondment/statistics2/blob/master/lib/statistics2/base.rb#L89
     */
    protected function pnorm($quantile){
        $b = [
            1.570796288, 
            0.03706987906, 
            -0.8364353589e-3, 
            -0.2250947176e-3,
            0.6841218299e-5, 
            0.5824238515e-5, 
            -0.104527497e-5, 
            0.8360937017e-7,
            -0.3231081277e-8, 
            0.3657763036e-10, 
            0.6936233982e-12
        ];

        if ($quantile < 0.0 || $quantile > 1.0 || $quantile == 0.5) return 0.0;

        $w1 = $quantile > 0.5 ? 1.0 - $quantile : $quantile;
        $w3 = - log(4.0 * $w1 * (1.0 - $w1));
        $w1 = $b[0];

        for ($i = 1; $i <= 10; $i++) {
            $w1 += $b[$i] * pow($w3, $i);
        }
        
        return $quantile > 0.5 ? sqrt($w1 * $w3) : -sqrt($w1 * $w3);
    }

    /**
     * @return float
     */
    private function multiply() { 
        return 1 / (1 + (pow($this->z, 2) / $this->n)); 
    }

    /**
     * @return float
     */
    private function innerLeft() { 
        return $this->p + (pow($this->z, 2) / (2 * $this->n)); 
    }
    /**
     * @return float
     */
    private function innerRight() { 
        return $this->z * sqrt(($this->p * (1 - $this->p) / $this->n) + (pow($this->z, 2) / (4 * pow($this->n, 2)))); 
    }


}
