<?php

namespace Vera\Wilson\Tests;

class LowerBoundTest extends \PHPUnit\Framework\TestCase {

    public function testSetConfidenceConvertsFloat() {
        $wilson = new \Vera\Wilson\LowerBound;
        $wilson->setConfidence(0.40);

        $this->assertEquals(40, $wilson->confidence);
    }

    public function testSetConfidenceThrowsException() {
        $wilson = new \Vera\Wilson\LowerBound;

        try {
            $wilson->setConfidence(1.1);
            $this->expectException(\Exception::class);
        } catch (\Exception $e) {
            $this->assertIsObject($e);
        }
    }

    public function testSetConfigQuantile() {
        $wilson = new \Vera\Wilson\LowerBound;

        $this->assertEquals('1.9599639715843482',$wilson->z);
    }

    public function testAddRatingsToZero() {
        $wilson = new \Vera\Wilson\LowerBound;
        $wilson->addRatings(0, 0);

        $this->assertEquals(0, $wilson->n);
    }

    public function testLowerBound() {
        $wilson = new \Vera\Wilson\LowerBound;
        $wilson->addRatings(31, 6);

        $this->assertIsFloat($wilson->getLowerBound());
    }

    public function testGetScore() {
        $wilson = new \Vera\Wilson\LowerBound;
        $wilson->addRatings(31, 6);

        $this->assertIsArray($wilson->getScore(), print_r($wilson->getScore()));
    }

}
