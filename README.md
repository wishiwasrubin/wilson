# Wilson Lower Bound

PHP Implementation of Wilson's lower bound score equation. As described by [Evan Miller](http://www.evanmiller.org/how-not-to-sort-by-average-rating.html).

### Installation
    composer require vera/wilson

### Usage

```php
    require 'vendor/autoload.php';

    $wilson = new \Vera\Wilson\LowerBound;
    
    $wilson->setConfidence(0.95);
    // By default confidence will be set to 0.95 so this is not necessary

    $wilson->addRatings(31, 6);

    $wilson->getLowerBound(); //0.68863516412593
```